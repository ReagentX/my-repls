from itertools import combinations


def can_two_movies_fill_flight_itertools(movies, flight_length):
    pairs = combinations(movies, 2)
    return any([sum(x) == flight_length for x in pairs])


def can_two_movies_fill_flight_loop(movies, flight_length):
    for i in range(len(movies)):
        for j in range(i + 1, len(movies)):
            if movies[i] + movies[j] == flight_length:
                return True
    return False


def can_two_movies_fill_flight(movies, flight_length):
    options = set()
    for i in movies:
        if flight_length - i in options:
            return True
        else:
            options.add(i)
    return False


print(can_two_movies_fill_flight([2, 4], 1))
print(can_two_movies_fill_flight([2, 4], 6))
print(can_two_movies_fill_flight([1, 2, 3, 4, 5, 6], 7))
print(can_two_movies_fill_flight([6], 6))
print(can_two_movies_fill_flight([4, 3, 2], 5))
print(can_two_movies_fill_flight([3, 8], 6))
print(can_two_movies_fill_flight([1, 2, 5, 1, 2, 1, 5, 1, 2], 10))
