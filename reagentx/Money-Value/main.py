def money_value(s):
  try:
    return float(s.replace('$', '').replace(' ', ''))
  except ValueError:
    return 0.0


print(money_value("12.34"))
print(money_value(" $12.34"))
print(money_value("-$12.34"))
print(money_value("$-- 12.34"))
