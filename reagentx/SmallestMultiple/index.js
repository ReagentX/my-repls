function range (min, max){
  const arr = Array.from({length: max}, (v, k) => k + 1);
  return arr.filter(v => v > min - 1);
}

function gcd(a, b){
  let r = a % b
  // if r is not zero, return b
  // if r is zero, recurse with new values
  return !r ? b : gcd(b, r);
}

function lcm(a, b){
  return (a * b) / gcd(a, b)
}

function lcmRng (min, max){
  const a = range(min, max);
  a.forEach(n => {
    console.log(`${min}, ${n}`);
    min = lcm(min, n);
  });
  return min
}

console.log(lcmRng(1, 20))