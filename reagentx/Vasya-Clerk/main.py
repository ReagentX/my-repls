def tickets(people):
    count_25 = 0
    count_50 = 0
    for bill in people:
        if bill == 25:
            count_25 += 1
        elif bill == 50:
            if count_25 >= 1:
                count_50 += 1
                count_25 -= 1
            else:
                return 'NO'
        elif bill == 100:
            if count_50 >= 1 and count_25 >= 1:
                    # Cant make change from max bill, no need to track iter
                    count_25 -= 1
                    count_50 -= 1
            elif count_25 >= 3:
                count_25 -= 3
            else:
                return 'NO'
    return 'YES'
    
print(tickets([25, 50, 25, 100]))
