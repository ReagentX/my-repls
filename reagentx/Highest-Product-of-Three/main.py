def highest_product_of_3(numbers):
    '''
    Since we can only keep track of one value at a time
        (i.e. not a tuple of 3 values)
        we have to store each step separately
    That is, we need to keep records of each min and max
        because we can't build a collection as we global

    Unlike the instructions, we dont need to initialize anything here 
        since we do all the math down below anyway
    '''
    highest_product_of_3 = numbers[0]
    highest_product_of_2 = numbers[0]
    highest = numbers[0]
    lowest_product_of_2 = numbers[0]
    lowest = numbers[0]
    for i in range(len(numbers)):
        highest_product_of_3 = max(
                                    numbers[i] * highest_product_of_2,
                                    numbers[i] * lowest_product_of_2,
                                    highest_product_of_3
                                  )
        highest_product_of_2 = max(
                                    numbers[i] * lowest,
                                    numbers[i] * highest,
                                    highest_product_of_2
                                  )
        lowest_product_of_2 = min(
                                    numbers[i] * lowest,
                                    numbers[i] * highest,
                                    highest_product_of_2
                                 )
        highest = max(highest, numbers[i])
        lowest = min(lowest, numbers[i])
    return highest_product_of_3

print(highest_product_of_3([6, 1, 3, 5, 7, 8, 2]))  # 336
print(highest_product_of_3([-5, -1, -3, -2]))  # -6
print(highest_product_of_3([-10, 1, 3, 2, -10]))  # 300
