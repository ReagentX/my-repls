import timeit
from collections import Counter

def anagrams(word, words):
  letters = set(word)
  return [x for x in words if len(word) == len(x) and set(x) == letters]


def anagrams_slow(word, words):
  return [item for item in words if sorted(item)==sorted(word)]


def anagrams_counter(word, words):
    counts = Counter(word)
    return [w for w in words if Counter(w) == counts]

print(timeit.timeit("anagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada'])", setup='from __main__ import anagrams'))
print(timeit.timeit("anagrams_slow('abba', ['aabb', 'abcd', 'bbaa', 'dada'])", setup='from __main__ import anagrams_slow'))
print(timeit.timeit("anagrams_counter('abba', ['aabb', 'abcd', 'bbaa', 'dada'])", setup='from __main__ import anagrams_counter'))
