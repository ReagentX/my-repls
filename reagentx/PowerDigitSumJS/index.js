const calc = (n) =>
{
  total = 0;
  while (n)
  {
    // Last digit is whatever remains when we divide by 10
    // In JS, all numbers are floats, so we remove the decimal
    total += Math.trunc(n % 10);
    // Floor division
    n = Math.floor(n / 10);
  }
  return total;
}

calc(Math.pow(2, 1000));
