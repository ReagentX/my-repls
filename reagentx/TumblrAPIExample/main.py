import json
import requests
import requests_cache


requests_cache.install_cache()
BLOG = ''  # example.tumblr.com
KEY = ''  # Your API Key


def get_text_data():
  offset = 0
  res = []
  more_data = True
  while(more_data):
    url = f'https://api.tumblr.com/v2/blog/{BLOG}/posts/text?api_key={KEY}&offset={offset}'
    data = json.loads(requests.get(url).content)

    try:
      if not data['response']['posts']:
        break
    except TypeError:
      break

    print(f'Got {offset} posts.')
    res += [d['body'] for d in data['response']['posts']]
    offset += 20
  return res


def get_ask_data():
  offset = 0
  res = []
  more_data = True
  while(more_data):
    url = f'https://api.tumblr.com/v2/blog/{BLOG}/posts/answer?api_key={KEY}&offset={offset}'
    data = json.loads(requests.get(url).content)

    try:
      if not data['response']['posts']:
        break
    except TypeError:
      break

    print(f'Got {offset} posts.')
    res += [d['question'] for d in data['response']['posts']]
    offset += 20
  return res


# text_data = get_text_data()
ask_data = get_ask_data()

# with open("Output3.txt", "w") as text_file:
#   for d in text_data:
#     print(f"{d}\n", file=text_file)

with open("Output.txt", "w") as text_file:
  for d in ask_data:
    print(f"{d}\n", file=text_file)
