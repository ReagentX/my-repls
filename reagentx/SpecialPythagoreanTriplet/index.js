// a = k(m²-n²), b = k(2mn), c = k(m² + n²)
// Euclid's formula
// https://mathcs.clarku.edu/~djoyce/java/elements/bookX/propX29.html
const solve = s =>
{
  for (let m = 2; m < s; m += 1)
  {
    for (let n = 1; n < m; n += 1)
    {
      const a = m ** 2 - n ** 2;
      const b = 2 * m * n;
      const c = m ** 2 + n ** 2;
      if (a + b + c === s)
      {
        const k = s / (a + b + c);
        return k*a * k*b * k*c; 
      }
    }
  }
  console.log(`Impossible!`);
}

solve(1000);
