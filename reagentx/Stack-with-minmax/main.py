class Stack(object):

    def __init__(self):
        """Initialize an empty stack"""
        self.items = []

    def push(self, item):
        """Push a new item onto the stack"""
        self.items.append(item)

    def pop(self):
        """Remove and return the last item"""
        # If the stack is empty, return None
        # (it would also be reasonable to throw an exception)
        if not self.items:
            return None

        return self.items.pop()

    def peek(self):
        """Return the last item without removing it"""
        if not self.items:
            return None
        return self.items[-1]

class MaxStack(object):

    # Implement the push, pop, and get_max methods
    def __init__(self):
        self.items = Stack()
        self.maxes = Stack()
        self.mins = Stack()

    def push(self, item):
        self.items.push(item)
        self.maxes.push(max(item, self.maxes.peek()))
        self.mins.push(min(item, self.mins.peek()))

    def pop(self):
        if not self.items:
            return None
        else:
            self.maxes.pop()
            self.mins.pop()
            return self.items.pop()

    def get_max(self):
        return self.maxes.peek()

    def get_min(self):
        return self.mins.peek()


class Stack_MinMax(object):

    def __init__(self):
        """Initialize an empty stack"""
        self.items = []
        self.max_ele = 0
        self.min_ele = 0

    def push(self, item):
        """Push a new item onto the stack"""
        if not self.items:
            self.items.append(item)
            self.min_ele = item
            self.max_ele = item
        else:
            if item < self.min_ele:
                self.items.append(2 * item - self.min_ele)
                self.min_ele = item
            elif item > self.max_ele:
                self.items.append(2 * item + self.max_ele)
                self.max_ele = item
            else:
                self.items.append(item)

    def pop(self):
        """Remove and return the last item"""
        # If the stack is empty, return None
        # (it would also be reasonable to throw an exception)
        if not self.items:
            return None
        else:
            item = self.items.pop()
            if item >= self.min_ele and item <= self.max_ele:
                return item
            if item < self.min_ele:
                pass
            if item > self.max_ele:
                pass

    def peek(self):
        """Return the last item without removing it"""
        if not self.items:
            return None
        return self.items[-1]


s = Stack_MinMax()
s.push(3) 
s.push(5) 
print(s.min_ele)
s.push(2) 
s.push(1) 
print(s.min_ele)
s.pop() 
print(s.min_ele)
s.pop() 
s.peek()
