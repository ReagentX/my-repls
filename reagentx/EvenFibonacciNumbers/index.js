const seed = [1, 2];

function generateFib (ceiling) {
  let lim = seed[seed.length - 1];
  while (lim < ceiling) {
    lim = seed[seed.length - 1] + seed[seed.length - 2];
    seed.push(lim)
  }
  return seed.slice(0, seed.length - 1)
}

vals = generateFib(2000000);

console.log(vals.reduce((p, c) => {
  return c % 2 === 0 ? p + c : p + 0
}, initialValue=0));

let sum = 0
for (let i = 0; i < vals.length; i += 1){
  if (vals[i] % 2 === 0){
    sum += vals[i];
  }
}
console.log(sum);