def rgb(r, g, b):
  clamp = lambda x: min(255, max(x, 0))
  return '{:02x}{:02x}{:02x}'.format(clamp(r), clamp(g), clamp(b)).upper()

print(rgb(255,255,255))
print(rgb(0, 0, 0))
print(rgb(-20,275,125))
print(rgb(254,253,252))
