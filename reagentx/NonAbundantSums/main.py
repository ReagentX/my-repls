import math


LIMIT = 20161


def sum_divisors(n):
  # Efficient sum divisor function O(√n)
  # Get the sqrt of n
  t = math.sqrt(n)
  # If t is an integer it means we will count it twice 
  # (since a perfect square means the divisor occurs 2 times)
  # As a reuslt, we need to offset one of them
  s = -t if t.is_integer() else 1
  # Ignore non-primes 0 and 1, continue up until 
  for i in range(2, int(t) + 1):
    if n % i == 0:
      # If the number is a divisor of n, so is what it divides n into
      s += i + n / i
  return int(s)


def solve(lim):
  abundant = set()
  res = []
  for i in range(1, lim):
    # Use a set because list lookup is O(n) and set lookup is O(1)
    if sum_divisors(i) > i:
      abundant.add(i)
    # Create a generator of the values where i - j is True when i - j is an abundant number
    # For example, if we are on 30, if any of the numbers in the set subtracted from 30 are
    #   in the list abundant, it results in True as that indicates 30 can be made by adding
    #   two abundant numbers
    # any() returns True if anything in the generator is True, since we want the opposite
    #   i.e. a generator of all False, we use `not`
    if not any(i - j in abundant for j in abundant):
        res.append(i)
  print(f'Abundant numbers: {len(abundant)}')
  print(f'Nonabundant numbers: {len(res)}')
  # Return the total of the numbers that 
  return sum(res)

print(solve(LIMIT))
