const cache = {1: 1};
const collatzLengthRecursive = (n) =>
{
  if (cache[n] !== undefined) { return cache[n]; }
  if (n % 2 == 0)
  {
    cache[n] = 1 + collatzLengthRecursive(n / 2);
  }
  else
  {
    cache[n] = 2 + collatzLengthRecursive((3 * n + 1) / 2);
  }
  return cache[n];
}

const collatzLengthBrute = (n) => 
{
  let length = 0;
  while (n !== 1) 
  {
      if (n % 2 == 0) 
      {
          n /= 2;
          length += 1;
      } else 
      {
          n = (3 * n) + 1;
          length += 1;
      }
  }
  // The last item needs to be counted too 
  return length + 1; 
}

const getLongestSequenceRecursive = (ceiling) =>
{
  let max = 1;
  let res = 1;
  // The longest result will always be in the upper half
  // collatzLength(n) < collatzLength(2 * n)
  for (let i = ceiling / 2; i <= ceiling; i += 1)
  {
    length = collatzLengthRecursive(i);
    res = max > length ? res : i;
    max = max > length ? max : length;
  }
  return res;
}

const getLongestSequenceBrute = (ceiling) =>
{
  let max = 1;
  let res = 1;
  // The longest result will always be in the upper half
  // collatzLength(n) < collatzLength(2 * n)
  for (let i = ceiling / 2; i <= ceiling; i += 1)
  {
    length = collatzLengthBrute(i);
    res = max > length ? res : i;
    max = max > length ? max : length;
  }
  return res;
}

console.time('getLongestSequenceRecursive');
console.log(getLongestSequenceRecursive(1000000));
console.timeEnd('getLongestSequenceRecursive');

console.time('getLongestSequenceBrute');
console.log(getLongestSequenceBrute(1000000));
console.timeEnd('getLongestSequenceBrute');