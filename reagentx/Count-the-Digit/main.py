def count_digits(n, target):
    count = 0
    # Ensure we dont ignore the leading 0 since in Python 0 is falsy
    if n == 0 and target == 0:
        count += 1
    while n:
        if n % 10 == target:
            count += 1
        n //= 10
    return count

def nb_dig(n, d):
    print(n, d)
    return sum([count_digits(x ** 2, d) for x in range(0, n + 1)])

print(nb_dig(100000, 5))
