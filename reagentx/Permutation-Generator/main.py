def permute_generator(n):
  if len(n) < 1:
    yield n
  else:
    for i in permute_generator(n[1:]):
      for j in range(len(n)):
        yield i[:j] + n[0:1] + i[j:]

for i in permute_generator([2, 6, 7]):
  print(i)