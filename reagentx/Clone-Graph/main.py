class GraphNode:
    def __init__(self, val):
        self.val = val         # an integer
        self.neighbors = []    # a list of GraphNode nodes



create_node(val, neighbors):
    n = GraphNode(val)
    n.neighbors = neighbors


def clone_graph(node):
    if not node:
        return None
    newnode = node
    start = newnode.neighbors
    while start:
        start.pop(0)


node_in_small_graph = GraphNode(7)
for i in range(4):
    new_node = GraphNode(i)
    node_in_small_graph.neighbors.append(new_node)
    new_node.neighbors.append(node_in_small_graph)
node_in_small_graph.neighbors[0].neighbors.append(node_in_small_graph.neighbors[1])
node_in_small_graph.neighbors[1].neighbors.append(node_in_small_graph.neighbors[0])
print(clone_graph(node_in_small_graph) == node_in_small_graph)