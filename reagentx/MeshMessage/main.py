def find_shortest_path(network, sender, recipient):
    """Find the shortest path through a P2P network"""
    # Handle invalid inputs
    if sender not in network:
        raise ValueError(f'Invalid sender: {sender}')
    if recipient not in network:
        raise ValueError(f'Invalid recipient: {recipient}')
    
    # Handle network traversal
    head = network[sender]
    print(head)
    if recipient in head:
        return [sender, recipient]
    else:
        nodes = head
        while head:
            top = nodes.pop(0)
            if recipient in network[top]:
                return [sender, top, recipient]



# Possibly use a set for these so we can get O(1) lookups,
# For large contact lists this will be slow
network = {
    'Min'     : ['William', 'Jayden', 'Omar'],
    'William' : ['Min', 'Noam'],
    'Jayden'  : ['Min', 'Amelia', 'Ren', 'Noam'],
    'Ren'     : ['Jayden', 'Omar'],
    'Amelia'  : ['Jayden', 'Adam', 'Miguel'],
    'Adam'    : ['Amelia', 'Miguel', 'Sofia', 'Lucas'],
    'Miguel'  : ['Amelia', 'Adam', 'Liam', 'Nathan'],
    'Noam'    : ['Nathan', 'Jayden', 'William'],
    'Omar'    : ['Ren', 'Min', 'Scott']
}
print(find_shortest_path(network, 'Jayden', 'Adam'))
