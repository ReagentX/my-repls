def longestCommonPrefix(strs: list) -> str:
    # Handle some special cases
    if not strs:
        return ''
    if len(strs) == 1:
        return strs[0]

    # Assign prefix to start working on
    prefix = strs[0]

    # Compare each thing to the current prefix, stop if we run out of space
    for i in range(1, len(strs)):
        for j in range(0, len(prefix)):
            if j < len(strs[i]) and j < len(prefix):
                if strs[i][j] == prefix[j]:
                    continue
                else:
                    prefix = prefix[:j]
                    break
            else:
                if j == 0:
                    return ''
                prefix = prefix[:j]
    return prefix


print(longestCommonPrefix(["flower","flow","floight"]), '== flo')
print(longestCommonPrefix(['aaa', 'aa', 'a']), '== a')