def build_list(ns, lim):
  l = []
  for i in ns:
    t = []
    while i and len(l) < lim:
      t.append(i % 10)
      i //= 10
    l.extend(reversed(t))
  return l


def gta(limit, *args):
    # find the base_list first
    #your code here
    # I can't get out of my mind these words "Round Robin"
    print(build_list(args, limit))
    return False


gta(7, 123489, 5, 67)
