from itertools import product

ADJACENTS = ['08', '124', '2135', '326', '4157', '52468', '6359', '748', '85790', '968']


def get_pins(observed):
  options = []
  for c in observed:
    options.append(ADJACENTS[int(c)])
  return [('{}'*len(observed)).format(*x) for x in list(product(*options))]

print(get_pins('08'))
