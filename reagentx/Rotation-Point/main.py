def find_rotation_point_old(words):
    '''Basically iterative binary search, but with strings'''
    floor = 0
    ceiling = len(words) - 1
    first = words[0]

    while floor < ceiling:
        midpoint = int((floor + ceiling) / 2)
        if words[midpoint] < first:
            ceiling = midpoint
        else:
            floor = midpoint
        # Handle when there is no rotation point
        if floor + 1 == ceiling and first > words[ceiling]:
            return ceiling
        elif floor + 1 == ceiling and first < words[ceiling]:
            return 0


print(find_rotation_point(['ptolemaic', 'retrograde', 'supplant',
                            'undulate', 'xenoepist', 'asymptote',
                            'babka', 'banoffee', 'engender',
                            'karpatka', 'othellolagkage']))
print(find_rotation_point(['ptolemaic', 'retrograde', 'supplant']))
print(find_rotation_point(['ptolemaic', 'retrograde', 'supplant', 'asmptote', 'beta']))
print(find_rotation_point(['grape', 'orange', 'plum', 'radish', 'apple', 'banana', 'cocoa']))
