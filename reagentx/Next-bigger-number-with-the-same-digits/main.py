def next_bigger(n):
  digits = list(map(int, list(str(n))))
  prev = digits[0]
  min_v = 0
  min_found = False
  for ix, digit in enumerate(digits[::-1]):
    if digit > prev:
      prev = digit
    elif not min_found:
      min_v = digit
      print(min_v)
      prev = digit
      min_found = True
      temp = 9
      for i, d in enumerate(digits[ix:]):
        if d < min_v:
          continue
        else:
          temp = min(max(d, temp))
          print(temp)
        digits[ix - 1] = d
        digits[i + ix] = min_v
    elif min_found:
      print

  return digits


print(next_bigger(534976))
# print(next_bigger(531))
# print(next_bigger(135))
# print(next_bigger(2017))
