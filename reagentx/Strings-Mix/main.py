from string import ascii_lowercase


def mix(s1, s2):
    letters = set(s1)|set(s2)
    res = []
    for l in letters:
        if l not in ascii_lowercase:
            continue
        c1 = s1.count(l)
        c2 = s2.count(l)
        if c1 == c2 and c1 > 1:
            res.append(f'=:{l*c1}')
        if c1 > c2 and c1 > 1:
            res.append(f'1:{l*c1}')
        if c2 > c1 and c2 > 1:
            res.append(f'2:{l*c2}')
    res.sort()
    return '/'.join(sorted(res, reverse=True, key=len))


print(mix("Are they here", "yes, they are here"))
print(mix("Lords of the Fallen", "gamekult"))
print(mix("A generation must confront the looming ", "codewarrs"))
