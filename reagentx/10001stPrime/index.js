function soE(n) {
  // Generate a list of primes using the sieve of Eratosthenes
  sieve = Array.from({length: n}, (v, i) => 0);
  for (let x = 2; x <= n; x += 1) {
    if (sieve[x]) continue;
    for (let u = 2*x; u <= n; u += x) {
      sieve[u] = x;
    }
  }
  /*
  Map the indeces to a new array (to get the actual primes)
  Filter out the undefined entries, i.e. the nonprimes
  Slice 0 and 1 off since theyre not technically prime
  */
  return sieve.map((v, i) => {
    if (v == 0) {return i;}
  }).filter(i => i != undefined).slice(2);
}

function getNthPrime(n) {
  // First, generate a list of primes of the required length
  let primes;
  let lim = n * 2;

  // No way to know how many primes for a given lim, so we loop
  do {
    primes = soE(lim);
    lim = lim * 2;
  } while (primes.length < n);

  // Return the nth prime
  return primes[n];
}

getNthPrime(10000);