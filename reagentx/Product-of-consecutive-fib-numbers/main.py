def fib():
    """Generates the Fibonacci numbers, starting with 0"""
    x, y = 0, 1
    while 1:
        yield x
        x, y = y, x + y

def productFib_slow(n):
    f = fib()
    fn = next(f)
    fn1 = next(f)
    prod = fn * fn1
    while prod < n:
        fn = fn1
        fn1 = next(f)
        prod = fn * fn1
        if prod == n:
            return [fn, fn1, True]
    return [fn, fn1, False]


def productFib(prod):
    """Generates the Fibonacci numbers, starting with 0"""
    x, y = 0, 1
    while prod > x * y:
        x, y = y, x + y
    return [x, y, prod == x * y]


print(productFib(4895))
print(productFib(4896))