def validate_battlefield(field):
    coords = set()
    for i in range(len(field)):
        for j in range(len(field[i])):
            if field[i][j] == 1:
                coords.add((i, j))
    return battle(coords, [1, 1, 1, 1, 2, 2, 2, 3, 3, 4])
  
def battle(grid, fleet):
    return sum(fleet) == len(grid) and (fleet == [] or 
        any(battle(grid - p, fleet[:-1]) 
        for p in possibles(grid, fleet[-1])))

def possibles(grid, ship):
    print(f'Testing {ship}')
    print([[(r,c+i) for i in range(ship)]
        for r,c in grid])
    res = []
    for r, c in grid:
        coords = set()
        for i in range(ship):
            coords.add((r + i, c))
            coords.add((r, c + i))
        
        if coords <= grid:
            print(coords)
            res.append(coords)
    print(res)

    print([set(p) for p in [[(r+i,c) 
        for i in range(ship)] for r, c in grid] + [[(r,c+i) for i in range(ship)]
        for r,c in grid]])
    return([set(p) for p in [[(r+i,c) 
        for i in range(ship)] for r, c in grid] + [[(r,c+i) for i in range(ship)]
        for r,c in grid] if set(p) <= grid])


print(validate_battlefield(
		[[1, 0, 0, 0, 0, 1, 1, 0, 0, 0],
		 [1, 0, 1, 0, 0, 0, 0, 0, 1, 0],
		 [1, 0, 1, 0, 1, 1, 1, 0, 1, 0],
		 [1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		 [0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
		 [0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
		 [0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
		 [0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
		 [0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
		 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]))

# print(validate_battlefield(
# 		[[1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
#         [1, 1, 0, 0, 0, 0, 0, 0, 1, 0],
#         [1, 1, 0, 0, 1, 1, 1, 0, 1, 0],
#         [1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
#         [1, 0, 0, 0, 0, 0, 0, 0, 1, 0],
#         [0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
#         [0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
#         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
#         [0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
#         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]))