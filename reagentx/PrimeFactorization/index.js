const getNumberOfDivisors = (n) =>
{
    let divisors = 0;
    const sqrt = Math.sqrt(n);
    for (let i = 1 ; i <= sqrt ; i += 1)
    {
        if ( n % i == 0 )
        {
            divisors+=2;
        }
    }
    if ( sqrt * sqrt == n )
    {
        divisors--;
    }
    return divisors;
}

const factor = n => [...Array(n + 1).keys()].filter(i => n % i === 0);

function getPrimeFactorMultiples(val) {
  // Define our variables
  let factors = [];
  let factor = 2;

  // Stop if we are only divisible by ourself
  while (val != factor) {
    // If we divide evenly by the current factor, save it and set the value to test equal to the result
    if (val % factor === 0) {
      factors.push(factor);
      val = val / factor;
      // Reset factor to 1 since we add 1 so the next loop starts with 2
      factor = 1;
    }
    // Iterate factor
    factor += 1;
    // Escape the loop so we don't run off
    if (factor > val) break;
  }
  // If we get here, val = factor and we need to add that
  // factors.push(factor);
  const v = new Set(factors);
  let mult = 1;
  Array.from(v).forEach(i => {
    mult *= factors.filter(j => j === i).length + 1;
  });
  return mult;
}

num = 10;
console.log(getNumberOfDivisors(num));
console.log(getPrimeFactorMultiples(num));
