def merge_sort(a, b):
    merged = []
    while a or b:
        if a and b:
            if a[0] < b[0]:
                merged.append(a.pop(0))
            else:
                merged.append(b.pop(0))
        if not a and b:
            merged += b
            break
        if not b and a:
            merged += a
            break
    return merged


def merge_sorted_k(arrs):
    if len(arrs) < 2:
        return sorted(arrs)
    merged = []
    for i in arrs:
        if not merged:
            merged = i
            continue
        merged = merge_sort(merged, i)
    return merged

arr = [ [1, 3, 5, 7],
        [2, 4, 6, 8],
        [0, 9, 10, 11],
        [3, 4, 2, 7] ] ;
print(merge_sorted_k(arr))