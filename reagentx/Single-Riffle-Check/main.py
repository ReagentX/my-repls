def is_single_riffle_no_recurs(half1, half2, shuffled_deck):
    riffle = []
    for i in range(len(shuffled_deck)):
        # print(f'Step {i}')
        if half1 and half2 and half1[0] < half2[0]:
            riffle.append(half1.pop(0))
        elif half2 and half1 and half2[0] < half1[0]:
            riffle.append(half2.pop(0))
        elif half2 and not half1:
            riffle.append(half2.pop(0))
        elif half1 and not half2:
            riffle.append(half1.pop(0))
        elif not half1 and not half2:
            return False
        if riffle[i] != shuffled_deck[i]:
            return False
    return True


def is_single_riffle(half1, half2, shuffled_deck):
    if not shuffled_deck:
        return True

    if half1 and half1[0] == shuffled_deck[0]:
        half1.pop(0)
        shuffled_deck.pop(0)
        return is_single_riffle(half1, half2, shuffled_deck)

    if half2 and half2[0] == shuffled_deck[0]:
        half2.pop(0)
        shuffled_deck.pop(0)
        return is_single_riffle(half1, half2, shuffled_deck)
    
    return False


print(is_single_riffle_no_recurs([1, 4, 5], [2, 3, 6], [1, 2, 3, 4, 5, 6]))  # True
print(is_single_riffle_no_recurs([1, 5], [2, 3, 6], [1, 2, 6, 3, 5]))  # False

print(is_single_riffle([1, 4, 5], [2, 3, 6], [1, 2, 3, 4, 5, 6]))  # True
print(is_single_riffle([1, 5], [2, 3, 6], [1, 2, 6, 3, 5]))  # False