// Basically requires you know Binet’s formula

const fibDigits = n =>
{
  const phi = (1 + Math.sqrt(5)) / 2
  return Math.ceil(
    ((Math.log10(5) / 2) + (n - 1)) / Math.log10(phi)
  );
}

fibDigits(1000);
