def change_possibilities(amount, denominations):
    ways_to_make_n = [0] * (amount + 1)
    ways_to_make_n[0] = 1

    for i in range(0, len(denominations)):
        print('Checking coin value', denominations[i])
        for j in range(denominations[i], amount + 1):
            '''
            For each coin, iterate from that value to the max value
                We only need to iterate from that value because it
                cannot be used to make a smaller value than itself
                This removes the need for a logic check, i.e. 
                `if denominations[i] < j` when iterating, thus
                reducing our problem space
            At each step, add the preceding number of ways we can
                make the target amount; i.e. for target 10 and 
                amounts [1, 5, 10] we would add in place of 10:
                    a) 1 way to make 5 using just 1
                    b) 1 way to make 10 using just 1
                    c) 1 ways to make 5 + a
                    d) 3 ways to make 10: result of b + c
            '''
            ways_to_make_n[j] += ways_to_make_n[j - denominations[i]]
            print(j, ways_to_make_n)
    return ways_to_make_n[amount]


print(change_possibilities(12, [1, 5]))
