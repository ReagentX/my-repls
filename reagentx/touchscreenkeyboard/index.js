// https://open.kattis.com/problems/touchscreenkeyboard

var keeb = [
  'qwertyuiop',
  'asdfghjkl',
  'zxcvbnm',
];

const getPos = l => {
  // Iterate through the rows until we find the row, then return the column and row
  for (let r in keeb){
    const index = keeb[r].indexOf(l);
    if (index !== -1) {
      return {col: index, row: r};
    }
  }
}

const calculateDistance = (l1, l2) => {
  // Calculate the distance between 2 keys
  const pos1 = getPos(l1);
  const pos2 = getPos(l2);
  return Math.abs(pos1.row - pos2.row) + Math.abs(pos1.col - pos2.col);
}

const calculateDistancePerLetter = w => {
  // Convert to list so we can use forEach
  word = w.split('');
  let distance = 0;
  if (word.length > 0) {
    // Set the first char as the starting point
    let start = word[0];
    word.slice(1).forEach(next => {
      distance += calculateDistance(start, next);
      // Assign the current key as the start for the next key
      start = next;
    });
  }
  return distance;
}

const calculateDistancePerWord = (w1, w2) => {
  // Only execute if words are the same length
  if (w1.length === w2.length) {
    let distance = 0;
    for (let i = 0; i < w1.length; i += 1) {
      distance += calculateDistance(w1[i], w2[i]);
    }
    return distance;
  }
  throw new Error('Lengths do not match!');
}

const rankWords = (w, l) => {
  // Build a list of all the word `t` distances from the source word `w`
  const distances = l.map(t => {
    return {word: t, distance: calculateDistancePerWord(w, t)};
  });
  // Sort them from low to high distance
  return distances.sort((a, b) => a.distance - b.distance);
}

console.log(calculateDistancePerLetter('word'));
console.log(calculateDistancePerWord('ifpv', 'icpc'));
console.log(rankWords('ifpv', ['icpc', 'iopc', 'gdpc']));
