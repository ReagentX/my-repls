def merge(a, b):
  res = []
  merged_list_size = len(a) + len(b)
  while len(res) < merged_list_size:
    '''
    If a has elements and b does not OR
    if a has elements and the first a is 
    smaller than the first b, add first a
    '''
    if a and not b or a and a[0] < b[0]:
      res.append(a.pop(0))
    # Do the opposite of the above
    elif b and not a or b and b[0] < a[0]:
      res.append(b.pop(0))
    print(res)
  return res


my_list = [3, 4, 6, 10, 11, 15, 20]
alices_list = [1, 5, 8, 12, 14, 19]
print(merge(my_list, alices_list))
