def merge_ranges(times):
  res = []
  times = sorted(times)  # :(
  for i in times:
    start, end = i
    if not res:
      res.append(i)
    elif res[-1][1] >= start:
      # If the new start time is earlier than previous end time, save the new start and the largest end time
      tmp = (res[-1][0], max(end, res[-1][1]))
      res.pop()
      res.append(tmp)
    else:
      res.append(i)
  return res


print(merge_ranges([(0, 1), (3, 5), (4, 8), (10, 12), (9, 10)]))
print(merge_ranges([(0, 1), (9, 10), (3, 5), (4, 8), (10, 12)]))
print(merge_ranges([(1, 2), (2, 3)]))
print(merge_ranges([(1, 5), (2, 3)]))
print(merge_ranges([(1, 10), (2, 6), (3, 5), (7, 9)]))
