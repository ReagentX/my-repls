def positive_sum(lst):
    positive_list = []
    for n in lst:
        if n >= 0:
            positive_list.append(n)
    return sum(positive_list)


a = [1,-4,7,12,2,-12,3]
# print(a)
print(positive_sum(a))

ps = lambda big_list: sum([n for n in big_list if n >= 0])
print(ps(a))

even_hundred = [x for x in range(100) if x % 2 == 0]
print(even_hundred)

even_hundred = {x: (x / 5) for x in range(11) if x % 2 == 0}
print(even_hundred)