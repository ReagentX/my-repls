const genSpiral = (x, y) =>
{
  // x represents columns, y represents rows
  const max = x * y;
  let topLeft = max - (x - 1)
  const rows = [];
  for (i = 0; i < y; i += 1)
  {
    rows.push([topLeft]);
    topLeft -= 1;
  }
  rows.forEach((row, rowNum) =>
  {
    if (rowNum === 0)
    {
      newArray = []
      for (i = 0; i < y; i += 1) { newArray.push(row[0] + i); }
      rows[rowNum] = newArray;
    }
    else if (rowNum === rows.length - 1)
    {
      newArray = []
      for (i = 0; i < y; i += 1) { newArray.push(row[0] - i); }
      rows[rowNum] = newArray;
    }
    else
    {
      newArray = [row[0]];
      let last = (row[0] - 2 * y) + rowNum - 1;
      for (i = 1; i < y - 1; i += 1) { newArray.push(row[0] - last - i); }
      newArray.push(last);
      rows[rowNum] = newArray;
    }
  });
  return rows;
}

genSpiral(5, 5);