def namelist(names):

  if len(names) == 0:
      return ''

  if len(names) == 1:
      return names[0]['name']

  names_list = [x['name'] for x in names]
  return ', '.join(names_list[:len(names_list) - 1]) + ' & ' + names_list[-1]

print(namelist([{'name': 'Bart'},{'name': 'Lisa'},{'name': 'Maggie'},{'name': 'Homer'},{'name': 'Marge'}]))