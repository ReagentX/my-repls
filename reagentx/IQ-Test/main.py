def iq_test(numbers):
    numbers = list(map(int, numbers.split(' ')))

    if len(numbers) <= 2:
      return 1

    previous_sign = numbers[0] % 2
    for i, v in enumerate(numbers):
        if v % 2 != previous_sign:
            # Handle when the sign change is in position 0
            if i == 1:
                if previous_sign != numbers[i + 1] % 2:
                    return i
            return i + 1

print(iq_test("2 4 66 88 55 66 88"))
print(iq_test("1 2 1 1"))
print(iq_test("2 1 1"))
print(iq_test("2 1"))