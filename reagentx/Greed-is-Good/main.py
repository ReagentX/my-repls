THREE_SCORE_MAP = {1: 1000, 2: 200, 3: 300, 4: 400, 5: 500, 6: 600}
ONE_SCORE_MAP = {1: 100, 5: 50}

def score(dice):
  score = 0  
  for opt in set(dice):
    triplet = False
    if dice.count(opt) >= 3:
      score += THREE_SCORE_MAP[opt]
      triplet = True
    if opt == 1 or opt == 5:
      extra = dice.count(opt) - 3 if triplet else dice.count(opt)
      score += ONE_SCORE_MAP[opt] * extra
  return score

print(score([5, 6, 1, 1, 1]))
print(score([4, 4, 4, 3, 3]))
print(score([2, 4, 4, 5, 4]))
