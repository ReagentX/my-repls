import random


def random_shuffle(cards):
    # As we iterate we will move the item at the place we are iterating over
    # That way we never move the same index twice
    for first in range(len(cards)):
        second = random.randint(first, len(cards) - 1)
        # while first == second:
        #     second = random.randint(0, len(cards) - 1)
        cards[first], cards[second] = cards[second], cards[first]
    return cards


cards = [1, 2, 3, 4, 5]
print(cards)
random_shuffle(cards)
print(cards)
random_shuffle(cards)
print(cards)
