def sum_pairs(ints, s):
  seen = set()
  for i in ints:
    if s - i in seen:
      return[s-i, i]
    seen.add(i)


l1= [1, 4, 8, 7, 3, 15]
l2= [1, -2, 3, 0, -6, 1]

print(sum_pairs(l1, 8))
print(sum_pairs(l2, -6))
