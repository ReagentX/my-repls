from collections import Counter

VALID = {4: 1, 3: 2, 2: 3, 1: 4}


def validate_battlefield(field):
    coords = []

    # Iterate through each row and column, add all the coordinates to a list
    for i in range(len(field)):
        for j in range(len(field[i])):
            if field[i][j] == 1:
                coords.append((i, j))

    # If there are not 20 coordinates, end
    if len(coords) != 20:
        return False

    blobs = []
    # Iterate through the coordinates list in totality
    for coord in coords[:]:
        # If the item has been removed already, skip it
        if coord not in coords:
            continue
        x, y = coord
        # Check diagonals, if any exist the board is invalid
        diagonals = {(x + 1, y + 1), (x + 1, y - 1), (x - 1, y - 1),
                     (x - 1, y + 1)}
        if [x for x in diagonals if x in coords]:
            return False

        # Create a set of coordinates for the connected items, assume we can go both ways
        blob = set()
        x_flag, y_flag = True, True

        # Since boats can be max 4 in length, iterate up to 3 times
        for i in range(1, 4):

            # If we can move vertically and the next coordinate exists, add it to the set
            if x_flag and (x + i, y) in coords:
                blob.add((x + i, y))
                coords.remove((x + i, y))
                y_flag = False
            else:
                blob.add(coord)
                x_flag = False

            # Do the same thing for the y axis
            if y_flag and (x, y + i) in coords:
                blob.add((x, y + i))
                coords.remove((x, y + i))
                x_flag = False
            else:
                blob.add(coord)
                y_flag = False

        # Once we run out of places to move, append the blob to the list of blobs
        blobs.append(blob)

    # Aggregate the result to a dict of { len(): count }
    result = Counter(map(len, blobs))
    for ship in VALID:
        if VALID[ship] != result[ship]:
            return False
    return True


battleField = [[1, 0, 0, 0, 0, 1, 1, 0, 0, 0],
               [1, 0, 1, 0, 0, 0, 0, 0, 1, 0],
               [1, 0, 1, 0, 1, 1, 1, 0, 1, 0], 
               [1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0, 1, 0], 
               [0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0, 1, 0], 
               [0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 1, 0, 0], 
               [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]

print(validate_battlefield(battleField))

battleFieldBad = [
    [1, 1, 0, 0, 0, 1, 1, 0, 0, 0], [1, 0, 1, 0, 0, 0, 0, 0, 1, 0],
    [1, 0, 0, 0, 1, 1, 1, 0, 1, 0], [1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 1, 0], [0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 1, 0], [0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
]
print(validate_battlefield(battleFieldBad))
