def cakes(recipe, available):
  m = False
  for item in recipe:
    if item not in available:
      return 0
    if not m:
      m = available[item] // recipe[item]
    elif available[item] // recipe[item] >= 1:
      print(item, available[item] // recipe[item])
      m = min(m, available[item] // recipe[item])
    else:
      return 0
  return m


recipe = {"flour": 500, "sugar": 200, "eggs": 1}
available = {"flour": 1200, "sugar": 1200, "eggs": 5, "milk": 200}
# print(cakes(recipe, available))

recipe = {"apples": 3, "flour": 300, "sugar": 150, "milk": 100, "oil": 100}
available = {"sugar": 500, "flour": 2000, "milk": 2000}
# print(cakes(recipe, available))


recipe = {'flour': 300, 'oil': 100, 'milk': 100, 'apples': 3, 'sugar': 150}
available = {'flour': 2000, 'oil': 20, 'milk': 2000, 'apples': 15, 'sugar': 500}
print(cakes(recipe, available))