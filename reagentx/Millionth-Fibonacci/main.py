import math

def calc(n):
    phi = (1 + math.sqrt(5)) / 2
    if n < 0:
        return -1 * round(math.pow(phi, -n) / math.sqrt(5))
    else:
        return round(math.pow(phi, n) / math.sqrt(5))


print(calc(1000))
