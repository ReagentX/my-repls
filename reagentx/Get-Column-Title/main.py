def get_column_title(num):
    if not isinstance(num, int):
        raise TypeError
    if num <= 0:
        raise IndexError
    res = ''
    while num > 0:
        mod = (num - 1) % 26
        res = chr(mod + 65) + res
        num = (num - mod) // 26
    return res


print(get_column_title(28))
print(get_column_title(2398423))
