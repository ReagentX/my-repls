// Solved separately
function range (min, max){
  const arr = Array.from({length: max}, (v, k) => k + 1);
  return arr.filter(v => v > min - 1);
}

function sum (arr){
  return arr.reduce((p, c) => p + c);
}

function squareSums (min, max){
  const a = range(min, max);
  const b = a.map(i => i * i);
  return sum(b)
}

function sumSquare (min, max){
  const a = range(min, max);
  const summ = sum(a);
  return summ * summ;
}

// Solved using binomial theorem
function binomialTheorem (min, max){
  const sum = max * (max + 1) / 2;
  const sum_sq = (2 * max + 1) * (max + 1) * max / 6;
  return (sum * sum) - sum_sq;
}

console.log(sumSquare(1, 100) - squareSums(1, 100))
console.log(binomialTheorem(1, 100))