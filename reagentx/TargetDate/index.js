const date = new Date('2016-01-01');

const dateNbDays = (p, a, r) => {
  /* Calculates the number of days it takes to get to a required dollar amount from a given principle and interest rate */

  // Interest rate % given as whole number, not decimal, converted to daily interest `i`
  const i = r / (100 * 360);

  /* A = P(1+r/n)^t
  To solve for t, find our money multuplier
  Formula becomes ->  A/P = (1+r/n)^t
  */
  const goal = a / p;

  /* Solve our formula for t by taking logarithm of both sides
  Log(A/P) = t * Log((1+i))
  Then, solve for t:
  Log(A/P) / Log((1+i)) = t
  */
  const days = Math.ceil(Math.log(goal) / (Math.log(1 + i)));

  // Add the number of days to the given start date
  const end = new Date(date)
  end.setDate(end.getDate() + days);

  // Do some annoying date formatting
  return `${end.getFullYear()}-${('00' + (end.getMonth() + 1)).slice(-2)}-${('00' + end.getDate()).slice(-2)}`;
}

console.log(dateNbDays(100, 101, 0.98));
console.log(dateNbDays(100, 150, 2.00));
