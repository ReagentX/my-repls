from binarytree import BinaryTreeNode


def is_balanced(tree_root):
    # A tree with no nodes is superbalanced
    if tree_root is None:
        return True

    lengths = []  # Cannot use a set because we need to do indexing later, will never have more than 2 elements
    nodes = [(tree_root, 0)]  # Array of tuples (node, depth)
    while nodes:
        node, depth = nodes.pop()
        if not node.left and not node.right:
            # If we are on a leaf, check the depth
            if depth not in lengths:
                lengths.append(depth)
            if len(lengths) > 2 or len(lengths) == 2 and abs(lengths[0] - lengths[1]) > 1:
                return False
        else:
            # Handle child nodes
            # Add children to the list of nodes to check
            if node.left:
                nodes.append((node.left, depth + 1))
            if node.right:
                nodes.append((node.right, depth + 1))
    return True



tree = BinaryTreeNode(5)
left = tree.insert_left(8)
right = tree.insert_right(6)
left.insert_left(1)
left.insert_right(2)
right.insert_left(3)
right.insert_right(4)
print(is_balanced(tree))