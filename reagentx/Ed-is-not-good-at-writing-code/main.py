def sort_by_squares_linear(l):
    """
    Since this is sorted, the further we get from the center the larger the squares will be
    Since we are looking at a sorted list, 0 (or the closest integer to it) will be the middle
    We then move outwards from the middle, selecting the smaller square until we run out of items to check
    Then we move to whatever end of the list has extra values
    """
    le = len(l)
    split = 0

    # Find the "mid"point where we flip from negative to positive
    for i in range(le):
        if l[i] >= 0:
            break
        else:
            split = i

    # Keep track of where we are in the traversal, where h1 is the last index of the negative half
    h1 = split
    h2 = split + 1
    result = []  # Store the data here

    # While there are both negatives and positives
    while h1 >= 0 and h2 < le + 1:
        # h1 has smaller value
        if l[h1] ** 2 < l[h2] ** 2:
            result.append(l[h1] ** 2)
            h1 -= 1
        # h2 has smaller value
        else:
            result.append(l[h2] ** 2)
            h2 += 1

    # Handle when there are an unequal amount of negative (h1) and positive (h2) integers
    while h1 >= 0:
        result.append(l[h1] ** 2)
        h1 -= 1

    while h2 < le:
        result.append(l[h2] ** 2)
        h2 += 1

    return result


print(sort_by_squares_linear([-2, 1, 5]))
print(sort_by_squares_linear([-2, 1, 5, 8]))
print(sort_by_squares_linear([-2, -1, 1, 5]))
print(sort_by_squares_linear([-4, -3, -2, 1, 5]))
