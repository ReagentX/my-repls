def ed_sucks_at_code(l):
    le = len(l)
    split = 0

    for i in range(le):
        if l[i] >= 0:
            break
        else:
            split = i

    h1 = split
    h2 = split + 1
    result = [0] * 3
    while h1 >= 0 and h2 >= h1:
        if l[h1] ** 2 < l[h2] ** 2:
            result.appdend(l[h1] ** 2)
            h1 -= 1
        else:
            result.append(l[h2] ** 2)
            h2 += 1

    while h1 >= 0:
        result.append(l[h1] ** 2)
        h1 -= 1

    while h2 < le:
        result.append(l[h2] ** 2)
        h2 += 1

    return result


print(ed_sucks_at_code([-2, 1, 5]))
print(ed_sucks_at_code([-2, 1, 5, 8]))
print(ed_sucks_at_code([-2, -1, 1, 5]))
