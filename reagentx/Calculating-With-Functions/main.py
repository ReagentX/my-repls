# Operator optional; return value when no operator
def zero(operator=None):
  if operator:
    return operator(0)
  return 0

def one(operator=None):
  if operator:
    return operator(1)
  return 1

def two(operator=None):
  if operator:
    return operator(2)
  return 2

def three(operator=None):
  if operator:
    return operator(3)
  return 3

def four(operator=None):
  if operator:
    return operator(4)
  return 4

def five(operator=None):
  if operator:
    return operator(5)
  return 5

def six(operator=None):
  if operator:
    return operator(6)
  return 6

def seven(operator=None):
  if operator:
    return operator(7)
  return 7

def eight(operator=None):
  if operator:
    return operator(8)
  return 8

def nine(operator=None):
  if operator:
    return operator(9)
  return 9


# Operator call must contain value
def plus(num):
  return lambda x: x + num

def minus(num):
  return lambda x: x - num

def times(num):
  return lambda x: x * num 

def divided_by(num):
  return lambda x: x // num 


print(seven(times(five())))
'''
Flow:

seven(times(five()))
times(five()) -> lambda x: x * five()
    five() <- 5
seven() <- lambda x: x * 5
lambda seven(): seven() * 5
    seven() <- 7
seven() <- 7 * 5
'''

print(four(plus(nine())))
print(eight(minus(three())))
print(six(divided_by(two())))