// Generator to generate the next triangular number in the sequence
function* triangularNumberGenerator() {
  let n = 0
  while (true) {
    n += 1;
    yield (n * (n + 1)) / 2;
  }
}
const gen = triangularNumberGenerator()


// Get number of factors using prime factorization by root
const numDiv = (n) => {
  let divisors = 0;
  const sqrt = Math.sqrt(n);
  for (let i = 1 ; i <= sqrt ; i += 1) {
    if ( n % i === 0 ) {
      divisors += 2;
    }
  }
  if ( sqrt ** 2 === n ) {
    divisors--;
  }
  return divisors;
}

const solve = (lim) => {
  // Get the next value from the generator
  let n = gen.next().value;
  let curr = numDiv(n);
  // While the number of divisors is smaller than the target, continue with the next option
  while (curr <= lim) {
    n = gen.next().value;
    curr = numDiv(n);
  }
  return n;
}

solve(500);
