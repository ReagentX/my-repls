def pick_peaks(arr):
  # Only need to save positions since we have the data
  pos = []
  peak = 0
  for i in range(1, len(arr)):
    # If we are larger than the previous, mark it
    if arr[i] > arr[i - 1]:
      peak = i
    
    # If we are smaller than the previous and were at one point larger, save the mark
    elif arr[i] < arr[i - 1] and peak:
      pos.append(peak)
      peak = 0
  
  # Get the data from the saved positions
  return {'pos': pos, 'peaks': [arr[x] for x in pos]}


print(pick_peaks([1,2,3,6,4,1,2,3,2,1]))
print(pick_peaks([1,2,2,2,2,2,1]))
print(pick_peaks([2, 1, 3, 1, 2, 2, 2, 2]))