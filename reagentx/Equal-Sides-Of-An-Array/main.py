def find_even_index(l):
  for i in range(len(l)):
    if sum(l[0:i]) == sum(l[i + 1:]):
      return i
  return -1


print(find_even_index([1,2,3,4,3,2,1]))
print(find_even_index([1,100,50,-51,1,1]))
print(find_even_index([1,2,3,4,5,6]))