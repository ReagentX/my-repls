import math

def movie(card, ticket, perc):
  b_price = card + ticket
  times = 1
  while b_price >= ticket * times:
    times += 1
    b_price = math.ceil(card + ticket*(perc*(1 - perc ** times) / (1 - perc)))
  return times

print(movie(0, 10, 0.95))
print(movie(500, 15, 0.9))


for i in range(3):
  print(500, 15*(0.9*(1 - 0.9 ** i) / (1 - 0.9)))