import random
from collections import Counter

def sort_scores_manual(numbers, ceil):
    d = {}
    for i in numbers:
        d[i] = d.get(i, 0) + 1
    res = []
    for i in range(ceil):
        if i in d:
            for k in range(d[i]):
                res.append(i)
    return res


def sort_scores(numbers, ceil):
    d = Counter(numbers)
    res = []
    for i in range(ceil):
        if i in d:
            res.extend([i] * d[i])
    return res


print(sort_scores([37, 89, 41, 65, 91, 53], 100))
print(sort_scores([20, 10, 30, 30, 10, 20], 100))
print(sort_scores([37, 89, 41, 65, 91, 53], 100))
print(sort_scores([20, 10, 30, 30, 10, 20], 100))
print(sort_scores([random.randint(0, 101) for x in range(1000)], 100))
