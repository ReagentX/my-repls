def reverse_in_place(l):
  first = 0
  last = len(l) - 1
  while first < last:
    l[first], l[last] = l[last], l[first]
    first += 1
    last -= 1


# Works with odd length lists
list = [1, 2, 3, 4, 5]
print(list)
reverse_in_place(list)
print(list)

# Works with even length lists
list = [1, 2, 3, 4]
print(list)
reverse_in_place(list)
print(list)
