def fortune(principal: int, interest: float, withdrawl: float, years: int, inflation: float) -> bool:
    print(principal, interest, withdrawl, years, inflation)
    """
    Steps:
    1) Adjust `principal` by `interest`
    2) Subtract `withdrawl` from `principal`
    3) Adjust `withdrawl` by `inflation`
    """
    interest = interest / 100
    inflation = inflation / 100
    iter = 1
    while principal > 0 and iter < years:
        # Interest on principal
        principal *= 1 + interest
        principal = int(principal)

        # Withdraw
        principal -= withdrawl

        # Inflate withdrawl
        withdrawl *= 1 + inflation
        withdrawl = int(withdrawl)

        iter += 1
        print(iter, '\t', principal, withdrawl)
    
    return iter >= years and principal > 0


print(fortune(100000, 1, 2000, 15, 1), True)
print(fortune(100000, 1, 10000, 10, 1), True)
print(fortune(100000, 1, 9185, 12, 1), False)
print(fortune(9858055, 9, 657720, 21, 6), True)
