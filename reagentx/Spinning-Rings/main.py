def spinning_rings(inner_max, outer_max):
  inner, outer, count = inner_max, 1, 1
  while inner != outer:
    count += 1
    inner = inner - 1 if inner - 1 >= 0 else inner_max
    outer = outer + 1 if outer + 1 <= outer_max else 0
  print(count)
  return count


spinning_rings(2, 3)  # 5
spinning_rings(3, 2)  # 2
# spinning_rings_algebra(2**24, 3**15)  # why
