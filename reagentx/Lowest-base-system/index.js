const is_ones = (n, b) => {
  while (n) {
    if (n % b != 1) { return false }
    n = Math.floor(n / b);
  }
  return true;
}


const getMinBase = number => {
  radix = 2;
  s = Math.sqrt(number);
  for (i = 0; i < s; i += 1) {
    if (is_ones(number, radix)) {
      return radix;
    }
    radix += 1;
  }
  return number - 1;
}


console.log(getMinBase(10000000002))
console.log(getMinBase(1001001))
console.log(getMinBase(125002500050001))
