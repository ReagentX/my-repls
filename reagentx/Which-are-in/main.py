def in_array(array1, array2):
    res = set()
    for i in set(array2):
      for s in array1:
        if s in i:
          res.add(s)
    return sorted(list(res))


a1 = ["live", "arp", "strong"] 
a2 = ["lively", "alive", "harp", "sharp", "armstrong"]
print(in_array(a1, a2))