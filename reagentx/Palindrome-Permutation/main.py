from collections import Counter

def has_palindrome_permutation_collections(the_string):
    v = Counter(the_string)
    count_odds = 0
    for i in v:
        if v[i] % 2 != 0:
            count_odds += 1
    return count_odds <= 1


def has_palindrome_permutation_dict(the_string):
    v = {}
    for i in the_string:
        v[i] = v.get(i, 0) + 1
    count_odds = 0
    for i in v:
        if v[i] % 2 != 0:
            count_odds += 1
    return count_odds <= 1


def has_palindrome_permutation(the_string):
    v = set()
    for i in the_string:
        if i not in v:
            v.add(i)
        else:
            v.remove(i)
    return len(v) <= 1


print(has_palindrome_permutation('aabcd'))  # False
print(has_palindrome_permutation('aabcbcd'))  # True
print(has_palindrome_permutation('aabccbdd'))  # True
print(has_palindrome_permutation('aabbcd'))  # False