const genGrid = (x, y) => Array.from({length: x}, i => Array(y));

const factorial = (n) =>
{
  // Fast factorial function
  var res=1;
  for (var i = 2; i <= n; i += 1) { res *= i; }
  return res;
}

const getGridPaths = (x, y) =>
{
  // Binomial coefficient
  return factorial(x + y) / (factorial(x) * factorial(y))
}
getGridPaths(20, 20);
