from itertools import permutations


def valid(a):
  lengths = set()
  combos = set()
  players = set()
  for day in a:
    # Handle pairs
    flat = ''
    for x in day:
      flat += x

    # Handle player playing > 1 time
    lengths.add(len(flat))
    if len(lengths) > 1:
      print('len issue')
      return False

    if not players:
      for player in flat:
        players.add(player)
    else:
      for player in flat:
        if player not in players:
          return False

    # Handle combinations
    for i in range(0, len(flat), len(a[0][0])):
      this_combos = permutations(flat[i:i + len(a[0][0])], 2)
      for combo in this_combos:
        if combo in combos:
          return False
        combos.add(combo)
  return True


v1 = [
     ["AB", "CD"],
     ["AD", "BC"],
     ["BD", "AC"]]
print(valid(v1))

v2 = [
     ["ABC", "DEF"],
     ["ADE", "CBF"]]
print(valid(v2))

v3 = [['ABCD', 'EFGH', 'IJKL', 'MNOP', 'QRST'], ['AEIM', 'BJOQ', 'CHNT', 'DGLS', 'FKPR'], ['AGKO', 'BIPT', 'CFMS', 'DHJR', 'ELNQ'], ['AHLP', 'BKNS', 'CEOR', 'DFXQ', 'GJMT'], ['AFJN', 'BLMR', 'CGPQ', 'DEKT', 'HIOS']]
print(valid(v3))