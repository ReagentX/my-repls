const approxCube = (n, r) => Math.round(n ** (1 / r));

console.log(approxCube(64, 3));
console.log(approxCube(472741006443, 3));
console.log(approxCube(65991621053219768206433, 3));