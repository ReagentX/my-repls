def fib():
    x = 0
    y = 1
    while 1:
        yield x
        x, y = y, x + y

def gen_fib(n):
    if n < 0:
        raise ValueError
    f = fib()
    r = next(f)
    for i in range(n):
        r = next(f)
    return r


def fib_binet(n):
    if n < 0:
        raise ValueError
    phi = (1 + 5 ** 0.5) / 2.0
    return int(round((phi ** n - (1 - phi) ** n) / 5 ** 0.5))

print(gen_fib(0))  # => 0
print(gen_fib(1))  # => 1
print(gen_fib(2))  # => 1
print(gen_fib(3))  # => 2
print(gen_fib(4))  # => 3
