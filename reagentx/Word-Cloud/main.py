class WordCloudData(object):

    def __init__(self, input_string):
        self.words_to_counts = {}
        self.split_string(input_string)
        print(self.words_to_counts)


    def split_string(self, s):
        is_in_word = False
        start_index = 0
        for i in range(len(s)):
            # If we are not in a word currently and are starting one, store that
            if s[i].isalpha() and not is_in_word:
                is_in_word = True
                start_index = i
            elif not is_in_word:
                continue
            # We want to ignore posessive nouns
            elif s[i] == "'":
                continue
            # If the word ends on this char, add the word to the class's dict
            elif is_in_word and not s[i].isalpha():
                self.add_word_to_dict(s[start_index:i])
                is_in_word = False
            # If we are at the end of the string and in a word, add it
            elif is_in_word and i == len(s) - 1:
                self.add_word_to_dict(s[start_index:i + 1])


    def add_word_to_dict(self, word):
        # If lowercase is there and we are in title case, ignore title case
        if word.lower() in self.words_to_counts and word.istitle():
            self.words_to_counts[word.lower()] += 1
        # If we are in title case and see lowercase, kill title case and move to lowercase
        elif word.title() in self.words_to_counts and word.islower():
            self.words_to_counts[word.lower()] = self.words_to_counts[word.title()] + 1
            del self.words_to_counts[word.title()]
        else:
            self.words_to_counts[word] = self.words_to_counts.get(word, 0) + 1


v = WordCloudData("Allie's Bakery: Sasha's Cakes")
v2 = WordCloudData("Mmm...mmm...decisions...decisions")
v3 = WordCloudData("Allies allies Allies")
v4 = WordCloudData("This This This this's")
v5 = WordCloudData("She sells seashells by the seashore. The shells she sells are surely seashells. So if she sells shells on the seashore, I'm sure she sells seashore shells")
