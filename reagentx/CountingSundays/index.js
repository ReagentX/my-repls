const solve = (start, end) =>
{
  date = new Date(start)
  sundayFallsOnFirst = 0;
  while (date < end)
  {
    if (date.getDate() === 1) { sundayFallsOnFirst += 1; }
    date.setDate(date.getDate() + 7);
  }
  return sundayFallsOnFirst;
}

const firstSunday = new Date('6 Jan 1901 ');
const lastDate = new Date('31 Dec 2000')
solve(firstSunday, lastDate)
