function chain (num){
  const digits = num.toString().split('');
  const squares = digits.map(v => v ** 2);
  const sum = squares.reduce((p, c) => p + c);
  return sum;
}

function solve (ceil) {
  let count = 0;
  for (let i = 2; i < ceil; i += 1){
    let res = i
    do {
      res = chain(res);
      if (res === 89){
        count += 1;
        i += 1;
        res = 1;
      }
    } while (res != 1)
  }
  return count;
}

solve(100000)