// https://www.codewars.com/kata/maximum-length-difference/train/javascript

function mxdiflg1(a1, a2) {
    if (a1.length === 0 || a2.length === 0) { return -1;}

    function* genLen (l) {
      for (let i = 0; i < l.length; i += 1) {
        yield l[i].length;
      }
    }

    const m1 = Math.max(...[...genLen(a1)]) - Math.min(...[...genLen(a2)]);
    const m2 = Math.max(...[...genLen(a2)]) - Math.min(...[...genLen(a1)]);

    return Math.max(...[m1, m2]);
}

function mxdiflgAssignment(a1, a2) {
    if (a1.length === 0 || a2.length === 0) { return -1;}

    s1l = a1.map(w => w.length);
    s2l = a2.map(w => w.length);

    const m1 = Math.max(...s1l) - Math.min(...s2l);
    const m2 = Math.max(...s2l) - Math.min(...s1l);

    return Math.max(...[m1, m2]);
}

function mxdiflgIterator(a1, a2){
  let max = -1;
  a1.forEach(x => {
    a2.forEach(y => {
      max = Math.max(Math.abs(x.length - y.length), max);
    });
  });
  return max;
}

const s1 = ["hoqq", "bbllkw", "oox", "ejjuyyy", "plmiis", "xxxzgpsssa", "xxwwkktt", "znnnnfqknaz", "qqquuhii", "dvvvwz"];
const s2 = ["cccooommaaqqoxii", "gggqaffhhh", "tttoowwwmmww"];

console.log(mxdiflgGenerator(s1, s2));
console.log(mxdiflgAssignment(s1, s2));
console.log(mxdiflgIterator(s1, s2));
