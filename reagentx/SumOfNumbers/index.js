function GetSum(a, b)
{
  // Not needed, but this reduces time complexity and doesn't assign `i` or `sum` since we dont need to
  if (a === b) { return a; }

  let sum = 0
  for (let i = Math.min(a, b); i < Math.max(a, b) + 1; i += 1){
     sum += i;
  }
  return sum;
}

function GetSumTwo(a, b)
{
  // Not needed, but this reduces time complexity and doesn't assign `i` or `sum` since we dont need to
  if (a === b) { return a; }

  let minn = Math.min(a, b);
  let sum = 0
  while (minn <= Math.max(a, b)) {
    // Dumb tricc with unary operator
    sum += minn++;
  }
  return sum
}

console.log(GetSum(10,-1))
console.log(GetSumTwo(10,-1))

console.log(GetSum(1,1))
console.log(GetSumTwo(1,1))