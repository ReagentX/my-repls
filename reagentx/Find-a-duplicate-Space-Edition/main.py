def find_repeat_set(numbers):
    cache = set()
    for number in numbers:
        if number in cache:
            return number
        else:
            cache.add(number)
    return 0


def find_repeat_mutate(numbers):
    '''This one uses O(1) space and O(nlogn) time, but kills the input'''
    numbers.sort()
    for i in range(len(numbers) - 1):
        if numbers[i] == numbers[i + 1]:
            return numbers[i]
    return 0


def find_repeat(numbers):
    '''O(1), no mutation of input'''
    # We know the range is 1..len(numbers)
    floor = 1
    ceil = len(numbers) - 1
    while floor < ceil:
        midpoint = floor + ((ceil - floor) // 2)
        running_total = 0
        # Determine if the dupe is in the lower range
        for i in numbers:
            if i >= floor and i <= midpoint:
                running_total += 1
        
        # Assign new floor if the upper bound has the answer
        if running_total <= midpoint - floor + 1:
            floor = midpoint + 1  # Increment by 1 because 0 index
        # Assign new ceiling if lower bound has the answer
        else:
            ceil = midpoint
    return floor


print(find_repeat([1, 1]))
print(find_repeat([1, 2, 5, 5, 5, 5]))
print(find_repeat([4, 1, 4, 8, 3, 2, 7, 6, 5]))
print(find_repeat([1, 2, 3, 4, 5, 6, 7, 1]))
