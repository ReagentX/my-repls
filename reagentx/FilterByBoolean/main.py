import pandas as pd
import numpy as np


# The original DF
print('Original DataFrame:')
df = pd.DataFrame({'D': {0: 6, 1: 4, 2: 6},
                    'A': {0: 1, 1: 2, 2: 3},
                    'C': {0: 2, 1: 7, 2: 5},
                    'B': {0: 4, 1: 5, 2: 6}})
print (df)

# The array of Booleans
print('\nArray of Booleans:')
arr = np.array([True, False, False, True])
print (arr)

# Filtered DataFrame
print('\nFiltered DataFrame:')
new_df = df[df.columns[arr]]
print(new_df)

cols = new_df.columns.values
print('\nFiltered Columns:')
print(cols)
