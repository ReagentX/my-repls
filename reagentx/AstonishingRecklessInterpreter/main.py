def dbl_linear(n):
  # Create some placeholders
  u = [1]
  x, y = 0, 0

  # Generate values up to the required amount
  for i in range(0, n):
    # Generate both possibilities for what we add
    next_x = 2 * u[x] + 1
    next_y = 3 * u[y] + 1
    
    # If the next value for x is <= the next y, add x and incremeent the search value
    if next_x <= next_y:
      u.append(next_x)
      x += 1

      # Since we would already capture that value, increment both
      if next_x == next_y:
        y += 1

    # Otherwise, append only one
    else:
      u.append(next_y)
      y += 1

  return u[n]




print(dbl_linear(10))
print(dbl_linear(20))