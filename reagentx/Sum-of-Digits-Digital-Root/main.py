def digital_root(n):
  sum = 0
  while n:
    sum += n % 10
    n = n // 10
  if sum > 10:
    return digital_root(sum)
  return sum


print(digital_root(493193))