from functools import reduce

def mult(list):
  top_three = []
  for i in list:
    if len(top_three) != 3:
      top_three.append(i)
    else:
      if max(i, min(top_three)) == i:
        top_three.remove(min(top_three))
        top_three.append(i)

  ''' without reduce
  mult = 1
  for i in top_three:
    mult *= i

  return mult
  '''
  # c is current item, a is accumulator
  return reduce(lambda c, a: c * a, top_three)     


print(mult([2, 3, 4, 5]))
print(mult([6, 1, 9, 3, 7]))
print(mult([10, 3, 5, 6, 20]))
