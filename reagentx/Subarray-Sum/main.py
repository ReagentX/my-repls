def maxSequence(arr):
  global_max = 0
  local_max = 0
  for i, v in enumerate(arr):
    local_max += v
    if local_max < 0:
      local_max = 0
    global_max = max(local_max, global_max)
  return global_max

print(maxSequence([-2, 1, -3, 4, -1, 2, 1, -5, 4]))
