from num2words import num2words

total = 0
# Start at 1, end at 1000
for i in range(1, 1001):
  total += len(num2words(i)
    .replace('-', '')
    .replace(' ', ''))

print(total)
