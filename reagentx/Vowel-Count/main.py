def getCount(inputStr):
    v = {'a', 'e', 'i', 'o', 'u'}
    num_vowels = 0
    # your code here
    for i in inputStr:
        if i.lower() in v:
            num_vowels += 1
    return num_vowels

print(getCount("abracadabra"))