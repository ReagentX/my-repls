function mxdiflg1(a1, a2){
  let max = -1;
  a1.forEach(x => {
    a2.forEach(y => {
      max = Math.max(Math.abs(x.length - y.length), max);
    });
  });
  return max;
}

function mxdiflg2(a1, a2){
  let max = -1;
    for (let x of a1) {
      for (let y of a2) {
        max = Math.max(Math.abs(x.length - y.length), max);
      }
    }  
  return max;
}

const s1 = ["hoqq", "bbllkw", "oox", "ejjuyyy", "plmiis", "xxxzgpsssa", "xxwwkktt", "znnnnfqknaz", "qqquuhii", "dvvvwz"];
const s2 = ["cccooommaaqqoxii", "gggqaffhhh", "tttoowwwmmww"];

console.log(mxdiflg1(s1, s2));
console.log(mxdiflg2(s1, s2));