def int_value(str):
    # Possibly use regex?
    for i in str:
        if i.isdigit():
            return int(i) - 1
    raise ValueError('No integer!')


def order(sentence):
    if not sentence:
        return ''
    s = sentence.split(' ')
    a = [''] * (len(s))
    for word in s:
        a[int_value(word)] = word        
    return ' '.join(a)


print(order("is2 Thi1s T4est 3a"))