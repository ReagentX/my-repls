from collections import Counter

def is_anagram(a, b):
  if a == b:
    return True
  return len(a) == len(b) and Counter(a) == Counter(b)

def anagram_counter(words):
    total = 0

    # Slow, but each pass we need to recheck fewer and fewer anagrams
    for i in range(len(words)):
      for j in range(i + 1, len(words)):
        if i == j:
          continue
        else:
          if is_anagram(words[i], words[j]):
            total += 1
    return total




# print(anagram_counter(['dell', 'ledl', 'abc', 'cba']))
print(anagram_counter(['dell', 'ledl', 'lled', 'cba']))
