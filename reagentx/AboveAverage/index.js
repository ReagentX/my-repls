const aboveAverage = l => {
  // Get the total value of an array
  const sum = l.reduce((p, c) => p + c);
  // Get the average of the array
  const avg = sum / l.length;
  // Make a list of the values larger than average, get the length and divide to get % above average
  return (l.filter(s => s > avg)).length / l.length;
}

const format = (num) => `${(num * 100).toFixed(3)}%`

const t1 = [50, 50, 70, 80, 100];
const t2 = [100, 95, 90, 80, 70, 60, 50];
const t4 = [70, 90, 80];
const t5 = [70, 90, 81];
const t3 = [100, 99, 98, 97, 96, 95, 94, 93, 91];

console.log(format(aboveAverage(t1)));
console.log(format(aboveAverage(t2)));
console.log(format(aboveAverage(t4)));
console.log(format(aboveAverage(t5)));
console.log(format(aboveAverage(t3)));
