from itertools import combinations

def choose_best_sum(max_distance, num_towns, ls):
    best_sum = 0
    options = combinations(ls, num_towns)
    for option in options:
        s = sum(option)
        best_sum = max(best_sum, s if s <= max_distance else 0)
    return None if best_sum == 0 else best_sum 


xs = [100, 76, 56, 44, 89, 73, 68, 56, 64, 123, 2333, 144, 50, 132, 123, 34, 89]
print(choose_best_sum(230, 4, xs))
print(choose_best_sum(430, 5, xs))
print(choose_best_sum(430, 8, xs))
