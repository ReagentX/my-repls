opposites = {'N': 'S', 'S': 'N', 'E': 'W', 'W': 'E'}

def dirReduc(arr):
  res = []
  for d in arr:
    if not res:
      res.append(d)
    else:
      if res[-1][0] == opposites[d[0]]:
        res.pop()
      else:
        res.append(d)
  return res


a = ["NORTH", "SOUTH", "SOUTH", "EAST", "WEST", "NORTH", "WEST"]
print(dirReduc(a))
