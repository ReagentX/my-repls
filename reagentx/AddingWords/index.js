const instructions = 
[
  'def foo 3',
  'calc foo + bar =',
  'def bar 7',
  'def programming 10',
  'calc foo + bar =',
  'def is 4',
  'def fun 8',
  'calc programming - is + fun =',
  'def fun 1',
  'calc programming - is + fun =',
  'clear',
]
const operators = ['+', '-']
let names = {};

const reverseLookup = v => {
  /* Return the property that a value is stored under */
  if (Object.values(names).indexOf(v) !== -1){
    // Rules say we will not repeat values, so [0] is ok
    return Object.keys(names).filter(p => names[p] === v)[0];
  }
  return 'unknown';
}

const parse = a => {
  a.forEach(instruction => {
    parts = instruction.split(' ');
    switch (parts[0]){
      case 'def':
        names[parts[1]] = Number(parts[2]);
        break;

      case 'calc':
        res = 0;
        expression = []

        parts.slice(1).forEach(val => {
          if (operators.indexOf(val) !== -1) {
            expression.push(val);
          } else if (names[val] !== undefined) {
            expression.push(names[val]);
          } 
        });

        try {
          const res = eval(expression.join(''));
          console.log(parts.slice(1).join(' ') + ` ${reverseLookup(res)}`);
        }
        catch(err) {
            console.log(parts.slice(1).join(' ') + ' unknown');
        }
        break;

      default:
        names = {}
    }
  });
}

parse(instructions);