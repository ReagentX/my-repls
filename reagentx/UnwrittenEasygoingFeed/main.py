import re

def count_smileys(arr):
    return len(re.findall('(:|;)(-|~|)(\)|D)', '\n'.join(arr)))


def count_smileys_list(arr):
  if not arr:
    return 0
  eyes = set([":", ";"])
  noses = set(["", "-", "~"])
  mouths = set([")", "D"])
  count = 0
  for s in arr:
    if len(s) == 2:
      if s[0] in eyes and s[1] in mouths:
        count += 1
    if len(s) == 3:
      if s[0] in eyes and s[1] in noses and s[2] in mouths:
        count += 1
  return count


print(count_smileys([':)',':(',':D',':O',':;']))
print(count_smileys([';]', ':[', ';*', ':$', ';-D']))

print(count_smileys_list([':)',':(',':D',':O',':;']))
print(count_smileys_list([';]', ':[', ';*', ':$', ';-D']))
