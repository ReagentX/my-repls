times = [("year", 365 * 24 * 60 * 60), 
         ("day", 24 * 60 * 60),
         ("hour", 60 * 60),
         ("minute", 60),
         ("second", 1)]

def format_duration(seconds):
  if seconds == 0:
    return 'now'
  
  parts = []
  for name, tiemval in times:
    val = seconds // tiemval
    if val:
      if val > 1:
        name += 's'
      parts.append('{} {}'.format(val, name))
      seconds = seconds % tiemval
  if len(parts) > 1:
    return ', '.join(parts[:len(parts) - 1]) + ' and {}'.format(parts[-1])
  return parts[0]


print(format_duration(1))