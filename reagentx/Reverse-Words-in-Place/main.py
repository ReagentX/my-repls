def reverse_chars(l, first, last):
  while first < last:
    l[first], l[last] = l[last], l[first]
    first += 1
    last -= 1


def reverse_words(l):
  reverse_chars(l, 0, len(l) - 1)
  start = 0
  for i in range(len(l)):
    if l[i] == ' ':
      reverse_chars(l, start, i - 1)
      start = i + 1
    elif i == len(l) - 1:
      reverse_chars(l, start, i)


# Works with odd length lists
list = [ 'c', 'a', 'k', 'e', ' ',
         'p', 'o', 'u', 'n', 'd', ' ',
         's', 't', 'e', 'a', 'l' ]


print(' '.join(''.join(list).split(' ')[::-1]))
reverse_words(list)
print(''.join(list))
