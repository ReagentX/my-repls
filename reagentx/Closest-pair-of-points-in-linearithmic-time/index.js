// Given by problem
function calculateDistance(a, b) {
  var dx = (a[0] - b[0]);
  var dy = (a[1] - b[1]);
  return Math.sqrt(dx * dx + dy * dy);
}


const closestPair = (points, calculateDistance) => {
  points.sort()
  let min = Infinity
  let res = []
  for (let i = 0; i < points.length - 1; i += 1) {
    for (let j = i + 1; j < points.length; j += 1) {
      // If the difference of the xs or ys are larger than the min distance
      // those points cannot be the solution 
      if (Math.abs(points[i][0] - points[j][0]) > min) { continue; }
      if (Math.abs(points[i][0] - points[j][0]) > min) { continue; }
      distance = calculateDistance(points[i], points[j])
      if (distance < min) {
        min = distance
        res = [points[i], points[j]]
      }
    }
  }
  return res
}


var points = [
  [2,20], // A
  [3,8],
  [2,9] // B
];

var result = closestPair(points, calculateDistance);
result