def pred(n, arr):
    '''Assume that all numbers will be > 0'''
    loc_max = 0
    for i in arr:
        if i < n:
            loc_max = max(loc_max, i)
    return loc_max

print(pred(5, [1, 2, 3, 5, 6, 4, 7]))