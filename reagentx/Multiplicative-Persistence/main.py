def persistence(n):
  if n < 24:
    return 0

  # Since we multiply, start at 1 and not 0
  val = 1

  # While n is not 0, take the last digit
  while n:
    val *= n % 10
    n = n // 10

  # If we find the target, return
  if val < 10:
    return 1

  # If we are too large, recurse
  else:
    return persistence(val) + 1


print(persistence(39))
print(persistence(4))
print(persistence(25))
print(persistence(999))