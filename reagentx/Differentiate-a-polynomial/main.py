import re


def differentiate(equation, point):
  print(equation)
  eq = re.split('(\+|-).*?', equation)
  dif = []
  for part in eq:
    if '^' in part:
      left, right = part.split('^')
      if 'x' in left:
        if left == 'x':
          left = str(right + left)
          # dif.append(right + '*(' + str(point) + '**' + str(int(right) - 1) +')')
          dif.append(f'{right} * ({str(point)} ** {str(int(right) - 1)})')
        else:
          left = str(int(left.replace('x', '')) * int(right))
          right = str(int(right) - 1)
          if point < 0 and int(right) % 2 == 0:
            # dif.append(left + '*(' + str(-point) + '**' + right + ')')
            dif.append(f'{left} * ({str(-point)} ** {right})')
          else:
            # dif.append(left + '*(' + str(point) + '**' + right + ')')
            dif.append(f'{left} * ({str(point)} ** {right})')
    elif 'x' in part and '^' not in part:
      if part == 'x':
        dif.append('1')
      else:
        dif.append(part.replace('x', ''))
    elif part in ['+', '-']:
      dif.append(part)
    else:
      dif.append('0')
  print(' '.join(dif))
  return eval(' '.join(dif))


print(differentiate("x^2-x", 3))
print()
print(differentiate("-5x^2+10x+4", 3))
print()
print(differentiate('-7x^5+22x^4-55x^3-94x^2+87x-56', -3))