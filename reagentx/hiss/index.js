const mic = w => /ss/i.test(w) ? 'hiss' : 'no hiss';

console.log(mic('amiss'));
console.log(mic('octopuses'));
