import random
import datetime


def no_sort():
    a = []
    for i in range(32678):
        a.append(random.randint(1, 256))
    sum = 0
    start = datetime.datetime.now()
    for i in a:
        if i >= 128:
            sum += i
    end = datetime.datetime.now()
    print(sum, 'in:', (end - start))


def sort():
    a = []
    for i in range(32678):
        a.append(random.randint(1, 256))
    a.sort()
    sum = 0
    start = datetime.datetime.now()
    for i in a:
        if i >= 128:
            sum += i
    end = datetime.datetime.now()
    print(sum, 'in:', (end - start))


if __name__ == '__main__':
    no_sort()
    sort()
