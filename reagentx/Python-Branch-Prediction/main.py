import random


def no_sort():
    a = []
    for i in range(32678):
        a.append(random.randint(1, 256))
    sum = 0
    for i in a:
        if i >= 128:
            sum += i


def sort():
    a = []
    for i in range(32678):
        a.append(random.randint(1, 256))
    a.sort()
    sum = 0
    for i in a:
        if i >= 128:
            sum += i


if __name__ == '__main__':
    import timeit
    print(timeit.timeit("no_sort()", setup="from __main__ import no_sort", number=10))
    print(timeit.timeit("sort()", setup="from __main__ import sort", number=10))
