let data = require('./data')

const alphabet = ['A', 'B',
  'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

const values = alphabet.map((l, i) => (0.5) * (i + 1) * (i + 2));
const valueSet = new Set(values);

const nameValue = str =>
{
  return str.split('')
            .map(l => alphabet.indexOf(l) + 1)
            .reduce((p, c) => p + c);
}

data = data.filter(name => valueSet.has(nameValue(name)));
console.log(data.length);
