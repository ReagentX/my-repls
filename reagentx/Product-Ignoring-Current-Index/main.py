from functools import reduce

def get_products_of_all_ints_except_at_index_reduce(numbers):
    if len(numbers) < 2:
        raise ValueError()
    product_till_now = 1
    res = []
    for i in range(0, len(numbers)):
        if i == len(numbers) - 1:
            print(product_till_now)
            res.append(product_till_now)
        else:
            product_after_now = reduce(lambda x, y: x * y, numbers[i + 1:])
            print(product_till_now, product_after_now)
            res.append(product_till_now * product_after_now)
        product_till_now *= numbers[i]
    return res

def get_products_of_all_ints_except_at_index(numbers):
    if len(numbers) < 2:
        raise ValueError()
    
    product_until_index = []
    for i in range(len(numbers)):
        if not product_until_index:
            product_until_index.append(numbers[i])
        else:
            product_until_index.append(product_until_index[-1] * numbers[i])
    
    product_so_far = 1
    print(product_until_index)
    for i in range(len(numbers) - 1, -1, -1):
        product_until_index[i] *= product_so_far
        product_so_far *= numbers[i]
    return product_until_index

print(get_products_of_all_ints_except_at_index([1, 2, 3]))
print(get_products_of_all_ints_except_at_index([-1, -2, -3, -4]))
print(get_products_of_all_ints_except_at_index([3, 1, 2, 5, 6, 4]))
