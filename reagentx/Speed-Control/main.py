import math


def gps(s, x):
    highest_speed = 0
    for i in range(1, len(x)):
      distance = (x[i] - x[i - 1]) / s
      hourly_speed = math.floor((60 * 60) * distance)
      highest_speed = max(highest_speed, hourly_speed)
    return highest_speed


x = [0.0, 0.19, 0.5, 0.75, 1.0, 1.25, 1.5, 1.75, 2.0, 2.25]
s = 15
print(gps(s, x))