import random

def binary_search(target, l):
    m = l[len(l) // 2]
    if m == target:
        return  True
    elif len(l) == 1:
        return False
    elif target < m:
        return binary_search(target, l[:len(l) // 2])
    elif target >= m:
        return binary_search(target, l[len(l) // 2:])
    return False


def binary_search_iterate(target, l):
    '''Assumes sorted ascending'''
    floor = 0
    ceiling = len(l) - 1
    while floor <= ceiling:
        m = floor + (ceiling - floor) // 2
        if l[m] == target:
            return True
        if l[m] < target:
            floor = m + 1
        elif l[m] > target:
            ceiling = m - 1
    return False


print(binary_search(2, [1, 2, 3, 4]))
print(binary_search(69, sorted([random.randint(0, 100) for x in range(10000)])))

print(binary_search_iterate(2, [1, 2, 3, 4]))
print(binary_search_iterate(2, [1, 2, 3, 4, 5]))
print(binary_search_iterate(7, [1, 2, 3, 4, 5]))
print(binary_search_iterate(7, [1, 2, 3, 4]))
print(binary_search_iterate(69, sorted([random.randint(0, 100) for x in range(10000)])))
