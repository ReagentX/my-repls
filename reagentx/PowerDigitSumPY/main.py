def calcString(number):
  # Using slow string conversions
  total = 0
  for digit in str(number):
    total += int(digit)
  return total

def calcDivis(number):
  # Using only math
  total = 0
  while number:
    total += int(number % 10)  # Remove decimal
    number //= 10  # Floor division
  return total

print(calcString(2 ** 1000))
print(calcDivis(2 ** 1000))
