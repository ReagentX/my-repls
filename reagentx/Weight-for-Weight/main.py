def digit_sum(n):
    n = int(n)
    sum = 0
    while n:
        sum += n % 10
        n = n // 10
    return sum

def order_weight(set):
    if not set:
      return set
    '''
    1) sorted(set.split(' ') sorts the list so that the smallest numbers come first
      This means that smaller strings will be at the front and not swapped on further sorting
    2) Sorting that using the digit_sum func puts the digit sums in proper order without changing the order of the items that are equal already
    '''
    return ' '.join(sorted(sorted(set.split(' ')), key=digit_sum))

print(order_weight("2000 10003 1234000 44444444 9999 11 11 22 123"))
