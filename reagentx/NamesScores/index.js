const alphabet = ['A', 'B',
  'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

const names = require('./names')
names.sort()

const res = names.map((v, i) =>
{
  let score = 0;
  // Split the name into an array of letters
  return v.split('')
          // Reduce on each letter
          .reduce((p, c) => {
            // Get the value of the letter from the array
            const val = alphabet.indexOf(c) + 1;
            // Add it to the total score
            score += val
            // Multiply the score by the index and return it
            return score * (i + 1)
            // Use 0 to start at index 0 and not 1
          }, 0);
});
console.log(res.reduce((p, c) => p + c));
