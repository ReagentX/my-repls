"""
Write a function 'string shuffle(string s)' which rearranges the characters in the input so that the same character does not occur twice in a row (that is, next to each other). It should return the shuffled string, if it is not possible to rearrange the string, the function should return null.

Examples:
"ABBA" -> "ABAB" or "BABA"
"AA" -> null
"""
from collections import Counter


def shuffle_queue(s: str) -> str:
    # Can implement this manually
    res = ''
    v = Counter(s)
    a = []
    for k in v:
        a.append((k, v[k]))
    print(a)
    while a:
        letter = a.pop(0)
        res += letter[0]
        if letter[1] > 1:
            letter = (letter[0], letter[1] - 1)
            a.insert(1, letter)
        print(a)
    return res

def shuffle_sort(s: str) -> str:
    s = sorted(s)
    res = s[0]
    del s[0]

    while s:
        # False when move is not blocked
        head_to_head = s[0] == res[0]
        tail_to_tail = s[-1] == res[-1]
        head_to_tail = s[0] == res[-1]
        tail_to_head = s[-1] == res[0]

        if head_to_head and tail_to_tail and head_to_tail and tail_to_head:
            return None
        elif not head_to_head:
            # print('can append head_to_head')
            res = s[0] + res
            del s[0]
        elif not tail_to_tail:
            # print('can append tail_to_tail')
            res = res + s[-1]
            del s[-1]
        elif not head_to_tail:
            # print('can append head_to_tail')
            res = res + s[0]
            del s[0]
        elif not tail_to_head:
            # print('can append head_to_head')
            res = s[-1] + res
            del s[-1]
    return res

print(shuffle_sort('ABBA'))
print(shuffle_sort('AAAAABBCRRSTT'))
print(shuffle_sort('AA'))
