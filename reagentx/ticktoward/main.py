def tick_toward(start, target):
    x, y = start
    t_x, t_y = target
    steps = [start]
    while x != t_x or y != t_y:
      if x > t_x:
        x -= 1
      if x < t_x:
        x += 1
      if y > t_y:
        y -= 1
      if y < t_y:
        y += 1
      steps.append((x, y))
    return steps

print(tick_toward((5, 5), (5, 7)))