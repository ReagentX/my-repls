def strstr(haystack, needle):
    h = 0
    n = 0
    idx = 0
    while h < len(haystack):
        print('checking\t', h, haystack[h])
        if haystack[h] == needle[n]:
            idx = n if idx == 0 else idx
            print('f', idx)
            n += 1
        elif n > 0 and haystack[h] != needle[n]:
            idx = 0
            n = 0
            print('l', idx)
        if n == len(needle) - 1:
            return idx
        h += 1
    return -1

haystack = 'ssearchi'
needle = 'search'
print(strstr(haystack, needle), haystack.index(needle))