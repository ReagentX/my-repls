const gen = a => {
  res = []

  // Cannot use a forEach because of the case for 13
  // Cannot break out of a forEach
  for (const n of a){
     if (n[0] === 0 && n[1] === 0) {
      return res;
    } else if (n[0] + n[1] === 13) {
      return ['Never speak again.'];
    } else if (n[0] > n[1]) {
      res.push('To the convention.');
    } else if (n[0] < n[1]) {
      res.push('Left beehind.');
    } else if (n[0] === n[1]) {
      res.push('Undecided');
    }
  }
  return res;
}

data = [[17, 3], [13, 14], [8, 5], [10, 12], [0, 0]];
dataTwo = [[17, 3], [13, 14], [44, 44], [0, 0], [22, 12]];

// Should only print 'Never speak again'
console.log(gen(data));
// Should exit after the third item
console.log(gen(dataTwo));