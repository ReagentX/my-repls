def gcd(a, b):
  if b == 0:
    return a
  return gcd(b, a % b)


def lcm(a, b):
    return a * b / gcd(a, b)


def convertFracts(lst):
  lcm = 1
  for pair in lst:
    n, d = pair
    lcm *= int(d / gcd(lcm, d))
  return [[round(lcm * n / d), lcm] for n, d in lst]

# print(convertFracts([[1, 2], [1, 3], [1, 4]]))
print(convertFracts([[27115, 5262], [87546, 11111111], [43216, 255689]]))
