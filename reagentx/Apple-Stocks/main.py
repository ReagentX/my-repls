def get_max_profit_loops(prices):
    max_profit = 0
    for i in range(len(prices)):
        for j in range(i, len(prices)):
            max_profit = max(max_profit, prices[j] - prices[i])
    return max_profit

def get_max_profit(prices):
    min_price = prices[0]
    max_profit = prices[1] - prices[0]
    for i in range(1, len(prices)):
        max_profit = max(max_profit, prices[i] - min_price)
        min_price = min(min_price, prices[i])
    return max_profit

stock_prices = [10, 7, 5, 8, 11, 9]
print(get_max_profit(stock_prices))
stock_prices = [10, 7, 5, 3, 2, 1]
print(get_max_profit(stock_prices))
stock_prices = [10, 10, 10, 10]
print(get_max_profit(stock_prices))