const val = 100;

function getPrimeFactors (val) {
  // Define our variables
  let factors = [];
  let factor = 2;

  // Stop if we are only divisible by ourself
  while (val != factor){
    // If we divide evenly by the current factor, save it and set the value to test equal to the result
    if (val % factor === 0){
      factors.push(factor);
      val = val / factor;
      // Reset factor to 1 since we add 1 so the next loop starts with 2
      factor = 1;
    }
    // Iterate factor
    factor += 1;
    // Escape the loop so we don't run off
    if (factor > val) return factors;
  }
  // If we get here, val = factor and we need to add that
  factors.push(factor);
  return factors;
}

console.log(getPrimeFactors(1000000002))
console.log(getPrimeFactors(1001001001))