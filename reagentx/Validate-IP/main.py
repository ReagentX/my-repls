def is_valid_IP(strng):
    s = strng.split('.')
    if len(s) != 4:
        return False
    for i in s:
        if not i.isdigit() or len(i) > 1 and i.startswith('0') or int(i) > 255 or int(i) < 0:
            return False
    return True


print(is_valid_IP('12.255.56.1'))
print(is_valid_IP('12.34.56 .1'))
print(is_valid_IP('12.34.56.-1'))
print(is_valid_IP('123.045.067.089'))
print(is_valid_IP('1.2.3.4.5'))
