import math


def sum_divisors(n):
    t = math.sqrt(n)
    s = -t if t.is_integer() else 1 
    for i in range(2, int(t)+1):
        if n % i == 0: s += i + n/i
    return int(s)


def solve(floor, ceil):
  res = [sum_divisors(i) for i in range(floor, ceil)]
  amicableNumbers = []
  for i, v in enumerate(res):
    if v < ceil:
      if res[i] == v and res[v] == i and i != v:
        amicableNumbers.append(i + v)
  # Return the sum of all the amicable numbers
  return sum(list(set(amicableNumbers)))


print(solve(0, 10000))
