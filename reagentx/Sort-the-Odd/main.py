def sort_array(source):
  # Make a list of the odd values and sort
  odds = sorted([x for x in source if x % 2 != 0])

  # Keep track of the current odd we are on
  idx = 0

  # Iterate through the source, skip over evens
  for i, v in enumerate(source):
    if v % 2 == 0:
      continue

    # If we are odd, replace the value at that index with the index we are on in the sorted odds list
    else:
      source[i] = odds[idx]
      idx += 1
  return source


print(sort_array([5, 3, 2, 8, 1, 4]))
