from math import factorial


# From Euler 16
def calc(number):
  total = 0
  while number:
    print(number)
    total += int(number % 10)  # Remove decimal
    number //= 10  # Floor division
  return total

print(calc(42))