def decompose(n):
    goal = 0
    result = [n]
    while result:
      current = result.pop()
      goal += current**2
      print(result, goal)
      for i in range(current - 1, 0, -1):
          if goal - (i**2) >= 0:
              goal -= i**2
              result.append(i)
              if goal == 0:
                  return sorted(result)
    return None


# print(decompose(11))
print(decompose(12))
