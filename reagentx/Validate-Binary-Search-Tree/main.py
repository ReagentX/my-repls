from binarytreenode import BinaryTreeNode


def is_binary_search_tree(root):
    nodes = [(root, 0, 999)]
    while nodes:
        node, lower_bound, upper_bound = nodes.pop()
        if node.value < lower_bound or node.value > upper_bound:
            return False
        if node.left:
            nodes.append((node.left, lower_bound, node.value))
        if node.right:
            nodes.append((node.right, node.value, upper_bound))
    return True


tree = BinaryTreeNode(50)
left = tree.insert_left(30)
right = tree.insert_right(70)
left.insert_left(10)
left.insert_right(40)
right.insert_left(60)
right.insert_right(80)
print(is_binary_search_tree(tree))


tree = BinaryTreeNode(50)
left = tree.insert_left(30)
right = tree.insert_right(80)
left.insert_left(20)
left.insert_right(60)
right.insert_left(70)
right.insert_right(90)
print(is_binary_search_tree(tree))