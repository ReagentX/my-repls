from bst import BinaryTreeNode


def find_largest(root):
    # Returns the largest item moving right, otherwise itself
    while root:
        if not root.right:
            return root.value
        root = root.right


def find_second_largest(root):
    if root is None or not root.left and not root.right:
        raise ValueError()

    while root:
        # If there is only a left child, we only want the largest
        if root.left and not root.right:
            return find_largest(root.left)
        
        # Root is the second largest when it has only a right child that has no children 
        if root.right and not root.right.right and not root.right.left:
            return root.value
        root = root.right


tree = BinaryTreeNode(50)
left = tree.insert_left(40)
left_left = left.insert_left(30)
left_left_left = left_left.insert_left(20)
left_left_left.insert_left(10)
actual = find_second_largest(tree)
expected = 40
print(actual, expected)

tree = BinaryTreeNode(50)
left = tree.insert_left(30)
right = tree.insert_right(70)
left.insert_left(10)
left.insert_right(40)
right.insert_left(60)
right.insert_right(80)
actual = find_second_largest(tree)
expected = 70
print(actual, expected)

tree = BinaryTreeNode(50)
left = tree.insert_left(30)
right = tree.insert_right(70)
left.insert_left(10)
left.insert_right(40)
right.insert_left(60)
actual = find_second_largest(tree)
expected = 60
print(actual, expected)

tree = BinaryTreeNode(50)
left = tree.insert_left(30)
right = tree.insert_right(70)
left.insert_left(10)
left.insert_right(40)
right_left = right.insert_left(60)
right_left_left = right_left.insert_left(55)
right_left.insert_right(65)
right_left_left.insert_right(58)
actual = find_second_largest(tree)
expected = 65
print(actual, expected)

tree = BinaryTreeNode(50)
left = tree.insert_left(30)
tree.insert_right(70)
left.insert_left(10)
left.insert_right(40)
actual = find_second_largest(tree)
expected = 50
print(actual, expected)
