# TODO: complete this class

class PaginationHelper:

  # The constructor takes in an array of items and a integer indicating
  # how many items fit within a single page
  def __init__(self, collection, items_per_page):
    self.collection = collection
    self.items_per_page = items_per_page


  # returns the number of items within the entire collection
  def item_count(self):
    return len(self.collection)


  # returns the number of pages
  def page_count(self):
    div = self.item_count() / float(self.items_per_page)  # 2.7 compatibility
    return div if div % 1 == 0 else int(div + (1 - div % 1))


  # returns the number of items on the current page. page_index is zero based
  # this method should return -1 for page_index values that are out of range
  def page_item_count(self, page_index):
    if page_index >= self.page_count() or page_index < 0:
      return -1

    if page_index == self.page_count() - 1:
      return (self.item_count() - (page_index * self.items_per_page))
    else:
      return self.items_per_page


  # determines what page an item is on. Zero based indexes.
  # this method should return -1 for item_index values that are out of range
  def page_index(self, item_index):
    if item_index + 1 > self.item_count() or item_index < 0:
      return -1

    for i in range(1, self.page_count() + 1):
      if item_index <= i * self.items_per_page:
        return i - 1 # Zero index
    raise ValueError 


# Example
collection = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
helper = PaginationHelper(collection, 10)

print(helper.page_count())
print(helper.page_index(23))
print(helper.page_index(24))
print(helper.page_item_count(2))
print(helper.page_item_count(3))
print(helper.item_count())
