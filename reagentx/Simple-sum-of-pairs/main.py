def digit_sum(n):
    sum = 0
    while n:
        sum += n % 10
        n //= 10
    return sum


def nines(n):
    digits = 0
    while n >= 10:
        digits *= 10
        digits += 9
        n //= 10
    return digits


def solve(n):
    v = nines(n)
    return digit_sum(v) + digit_sum(n - v)

print(solve(10e10))
