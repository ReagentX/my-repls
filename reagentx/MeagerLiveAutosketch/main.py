def dsum(n):
  sum = 0
  for i in range(1, int(n ** (1 / 2)) + 1):
    if n % i == 0:
      sum += (i ** 2)
      if i ** 2 != n:
        sum += (n / i) ** 2
  return int(sum)

def list_squared(m, n):
  res = []
  for i in range(m, n):
    sum = dsum(i)
    if (sum ** (1 / 2)) % 1 == 0:
      res.append([i, sum])
  return res


print(list_squared(1, 250))
print(list_squared(250, 500))