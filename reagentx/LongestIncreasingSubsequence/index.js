const genSubSequence = sq => {
  /* An increasing subsequence is a set of numbers where n < n + 1 */
  let subsequence = [];
  let longestSubsequence = [];

  sq.forEach((n, i) =>
  {
    if (n < sq[i + 1])
    {
      // Add both if we are at the beginning, else add just the index
      if (subsequence.length === 0){ subsequence.push(i, i + 1); }
      else { subsequence.push(i + 1); }
    }
    else 
    {
      if (subsequence.length > longestSubsequence.length)
      {
        // Copy the subsequence if we are larger, then reset
        longestSubsequence = subsequence.slice();
        // Reset because we stored the longest one
        subsequence = [];
      }
      // Reset because the sequence is over
      subsequence = [];
    } 
  });
  console.log(longestSubsequence.length);
  console.log(longestSubsequence);
}

genSubSequence([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
genSubSequence([1, 2, 3, 1, 2, 3, 4, 5, 1, 2])
genSubSequence([5, 19, 5, 81, 50, 28, 29, 1, 83, 23])
