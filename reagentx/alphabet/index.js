const removeCharAt = (s, p) => {
  return s.slice(0, p) + s.slice(p + 1);
}

const alpha = s => {
  let res = ''

  for (let i = 0; i < s.length - 1; i += 1){
    // console.log(`${s[i]} > ${s[i + 1]} => ${s[i] > s[i+1]}`)
    if (s[i] > s[i+1]) {
      // console.log(`Remove ${s[i]}`);
      return alpha(removeCharAt(s, i));
    }
  }
  // console.log(s);
  return 26 - s.length;
}

console.log(alpha('xyzabcdefghijklmnopqrstuvw'));
console.log(alpha('aiemckgobjfndlhp'));